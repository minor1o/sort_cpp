#ifndef KHW__LIBSORT_H_
#define KHW__LIBSORT_H_

#include <bits/stdc++.h>
#define RC(V) (((v) << 1)+1)
#define LC(V) ((v) << 1)
const int ktests = 50;
static const std::vector<int> N = {19, 101, 509, 2003,
                                   10007, 20011, 50021};
//, 100003, 1000003};
void mergesort(std::vector<int> &a);

namespace Sort_helper {
template<typename Arg, typename... Args>
void write(std::ostream &out, Arg &&arg, Args &&...args) {
  out << std::forward<Arg>(arg);
  ((out << std::forward<Args>(args)), ...);
}
void output_sorts(const char *opath,
                  const std::vector<std::string> &func_names,
                  const std::vector<std::vector<double>> &times) {
  std::ofstream output(opath, std::ios::out);
  write(opath ? output : std::cout, "n\\f_sort", ",");
  for (int i = 0; i < func_names.size(); ++i) {
    write(opath ? output : std::cout, func_names[i], ((i != times.size() - 1) ? ',' : '\n'));
  }
  for (int n = 0; n < N.size(); ++n) {
    write(opath ? output : std::cout, N[n], ',');
    for (int i = 0; i < times.size(); ++i) {
      write(opath ? output : std::cout, times[i][n], ((i != times.size() - 1) ? ',' : '\n'));
    }
  }
}
void merge(std::vector<int> &a, int l, int m, int r) {
  std::vector<int> lh(m - l + 1), rh(r - m);
  for (int i = 0; i < lh.size(); ++i) lh[i] = a[i + l];
  for (int i = 0; i < rh.size(); ++i) rh[i] = a[i + m + 1];

  int i = 0, j = 0, k = l;
  while (i < lh.size() && j < rh.size())
    a[k++] = (lh[i] > rh[j]) ? lh[i++] : rh[j++];
  while (i < lh.size())
    a[k++] = lh[i++];
  while (j < rh.size())
    a[k++] = rh[j++];
}
void recmergesort(std::vector<int> &a, int l, int r) {
  if (l < r) {
    int m = l + ((r - l) >> 2);
    recmergesort(a, l, m);
    recmergesort(a, m + 1, r);
    merge(a, l, m, r);
  }
}
void heap_sift_down(std::vector<int> &h, int v) {
  while ((LC(v)) < h.size()) {
    int x = LC(v);
    if (RC(v) < h.size() && h[RC(v)] > h[x])
      x = RC(v);
    if (h[v] >= h[x]) break;
    std::swap(h[v], h[x]);
    v = x;
  }
}
void heapify(std::vector<int> &h, int n) {
  for (int v = n; v > 0; --v)
    heap_sift_down(h, v);
}
int heap_erase_min(std::vector<int> &h) {
  assert(h[0] > 0);
  int res = h[1];
  h[1] = h[h[0]];
  h[0]--;
  if (h[0] > 0) heap_sift_down(h, 1);
  return res;
}
void recqsort(std::vector<int> &a, int l, int r) {
  if (l >= r) return;
  int opora_ind = l;
  for (int i = l + 1; i <= r; ++i) {
    if (a[i] <= a[l]) ++opora_ind;
  }
  std::swap(a[opora_ind], a[l]);
  for (int i = l, j = r; i < opora_ind & j > opora_ind;) {
    while (a[i] <= a[opora_ind]) ++i;
    while (a[j] > a[opora_ind]) --j;
    if (i < opora_ind && j > opora_ind) {
      std::swap(a[i++], a[j--]);
    }
  }
  recqsort(a, l, opora_ind - 1);
  recqsort(a, opora_ind + 1, r);
}
void go(int &a, int &b) {
  if (a > b) std::swap(a, b);
}
int sort7(std::vector<int> &arr, int l, int n) {
  if (n != 5) {
    std::vector<int> tmp(n);
    for (int i = 0; i < n; ++i) tmp[i] = arr[l + i];
    std::sort(tmp.begin(), tmp.end());
    for (int i = 0; i < n; ++i) arr[l + i] = tmp[i];
    return arr[l + (n >> 1)];
  }
  go(arr[l], arr[l + 1]);
  go(arr[l + 2], arr[l + 3]);

  if (arr[l] > arr[l + 2]) {
    std::swap(arr[l], arr[l + 2]);
    std::swap(arr[l + 1], arr[l + 3]);
  }
  if (arr[l + 2] > arr[l + 4]) {
    //4comp
    std::swap(arr[l + 3], arr[l + 4]); // cde -> ced
    std::swap(arr[l + 2], arr[l + 3]); // ced -> ecd
    if (arr[l] > arr[l + 2]) { //a > e
      //5 comp
      std::swap(arr[l + 1], arr[l + 2]); // abecd -> aebcd
      std::swap(arr[l], arr[l + 1]); // aebcd -> aebcd
      go(arr[l + 2], arr[l + 3]); //aebcd \/ aecbd
      if (arr[l + 3] > arr[l + 4]) {
        std::swap(arr[l + 3], arr[l + 4]);
//        std::swap(c, d);
      }
    }
    else { // a <= e
      //5 comp
      if (arr[l + 1] > arr[l + 3]) { //b > c
        //6 comp
        std::swap(arr[l + 1], arr[l + 2]);
        std::swap(arr[l + 2], arr[l + 3]);
        go(arr[l + 3], arr[l + 4]);
        //7 comp
      }
      else { // b <= c
        //6 comp
        go(arr[l + 1], arr[l + 2]);
      }
    }
    //6 comp
    return arr[l + 2];
  }
  // else { c <= e }
  //4 comp
  if (arr[l + 3] > arr[l + 4]) {
    //5 comp
    std::swap(arr[l + 3], arr[l + 4]); //cde -> ced
    if (arr[l + 1] > arr[l + 3]) { // b > e
      //6 comp
      std::swap(arr[l + 1], arr[l + 2]); //abced -> acbed
      std::swap(arr[l + 2], arr[l + 3]); //acbed -> acebd
      if (arr[l + 3] > arr[l + 4]) {
        //7 comp
        std::swap(arr[l + 3], arr[l + 4]);
//        std::swap(arr[l + 2], arr[l + 3]);
      }
    }
    else {
      //6 comp
      go(arr[l + 1], arr[l + 2]); // abced \/ acbed
      //7 comp
    }
    return arr[l + 2];
  }
  // else // cde (c <= d <= e)
  //5 comp
  if (arr[l + 1] > arr[l + 3]) { // b > d
    //6 comp
    std::swap(arr[l + 1], arr[l + 2]); //abcde -> acbde
    std::swap(arr[l + 2], arr[l + 3]); //acbde -> acdbe
    go(arr[l + 3], arr[l + 4]); //acdbe \/ acdeb
    //7 comp
  }
  else { // b<=d
    //6 comp
    go(arr[l + 1], arr[l + 2]);
    //7 comp
  }
  return arr[l + 2];
}
int split(std::vector<int> &a, int l, int r, int val) {
  int res;
  for (res = l; res < r; ++res) {
    if (a[res] == val)
      break;
  }
  std::swap(a[res], a[r]);

  res = l;
  for (int i = l; i < r; ++i) {
    if (a[i] <= val) {
      std::swap(a[i], a[res++]);
    }
  }
  std::swap(a[res], a[r]);
  return res;
}
int opornik(std::vector<int> &a, int l, int r, int ind) {
  int sz = r - l + 1;
  std::vector<int> med((sz + 4) / 5);

  for (int i = 0; i < sz / 5; i++)
    med[i] = sort7(a, l + i * 5, 5);

  if (sz % 5) med[sz / 5] = sort7(a, l + (sz + 4) / 5 - 1, sz % 5);

  int median_among_medians = med[0];
  if (med.size() != 1)
    median_among_medians = opornik(med, 0, med.size() - 1, med.size() / 2);

  int opora_med = split(a, l, r, median_among_medians);
  if (opora_med == l + ind - 1)
    return a[opora_med];

  return (opora_med > l + ind - 1) ? opornik(a, l, opora_med - 1, ind) :
         opornik(a, opora_med + 1, r, ind - opora_med + l - 1);
}
void nlognrecsort(std::vector<int> &a, int l, int r) {
  if (l >= r) return;
  int val = opornik(a, l, r, (r - l + 1) >> 1);
  int opora_ind = split(a, l, r, val);
  nlognrecsort(a, l, opora_ind - 1);
  nlognrecsort(a, opora_ind + 1, r);
}

std::vector<int> gen(int n) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<int> distr(INT_MIN + 1, INT_MAX - 1);
//  std::uniform_int_distribution<int> distr(-100, 100);
  std::vector<int> a(n);
  for (auto &e : a) e = distr(rng);
  return a;
}

int join(std::vector<int> &arr, std::vector<int> &left, std::vector<int> &right,
         int l, int m, int r) {
  int i;
  for (i = 0; i <= m - l; i++)
    arr[i] = left[i];

  for (int j = 0; j < r - m; j++) {
    arr[i + j] = right[j];
  }
}

int split(std::vector<int> &arr, std::vector<int> &left, std::vector<int> &right,
          int l, int m, int r) {
  for (int i = 0; i <= m - l; i++)
    left[i] = arr[i * 2];

  for (int i = 0; i < r - m; i++)
    right[i] = arr[i * 2 + 1];
}

// Function to generate Worst Case
// of Merge Sort
void generateWorstCase(std::vector<int> &arr, int l, int r) {
  if (l <= r) return;
  int m = l + (r - l) / 2;

  // Create two auxiliary arrays
  std::vector<int> left(m - l + 1);
  std::vector<int> right(r - m);

  // Store alternate array elements
  // in left and right subarray
  split(arr, left, right, l, m, r);

  // Recurse first and second halves
  generateWorstCase(left, l, m);
  generateWorstCase(right, m + 1, r);

  // Join left and right subarray
  join(arr, left, right, l, m, r);
}
std::vector<int> specialgen(int n, void (*f)(std::vector<int> &)) {
  std::vector<int> a = gen(n);
  std::sort(a.begin(), a.end());
  if (f == mergesort) {
    generateWorstCase(a, 0, a.size() - 1);
    return a;
  }
  int r = rand() % 3;
  if (!r) return {n, rand()};
  if (r == 1) std::reverse(a.begin(), a.end());
  return a;

}
}
std::vector<double> testme(void (*f)(std::vector<int> &)) {
  std::vector<double> res;
  res.reserve(N.size());
  for (const auto &n : N) {
    std::vector a = Sort_helper::gen(n);
//    std::vector a = Sort_helper::specialgen(n, f);
    //special generator to brake sort algos
    double alltime = .0;
    for (int k = 0; k < ktests; ++k) {
      std::vector<int> c = a;
      auto start = std::chrono::high_resolution_clock::now();
      f(c);
      std::chrono::duration<double> diff = std::chrono::high_resolution_clock::now() - start;
      alltime += diff.count();
    }
    res.push_back(alltime / ktests);
  }
  return res;
}

void insertsort(std::vector<int> &a) {
  for (int i = 1; i < a.size(); ++i) {
    int tmp = a[i], j = i - 1;
    for (; j >= 0 && a[j] < tmp; --j)
      a[j + 1] = a[j];
    a[j + 1] = tmp;
  }
}
void mergesort(std::vector<int> &a) {
  Sort_helper::recmergesort(a, 0, a.size() - 1);
}
void stoodqsort(std::vector<int> &a) {
  std::sort(a.begin(), a.end());
}
void myqsort(std::vector<int> &a) {
  Sort_helper::recqsort(a, 0, a.size() - 1);
}
void heapsort(std::vector<int> &a) {
  std::vector<int> h(a.size() + 1);
  h[0] = a.size();
  std::copy(a.begin(), a.end(), h.begin() + 1);
  Sort_helper::heapify(h, h[0]);
  for (int i = a.size() - 1; i >= 0; --i)
    a[i] = Sort_helper::heap_erase_min(h);
}
void nlognsort(std::vector<int> &a) {
  Sort_helper::nlognrecsort(a, 0, a.size() - 1);
}

//void skiplistsort() {}

#endif //KHW__LIBSORT_H_
