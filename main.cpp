#include "libsort.h"
int main(int argc, char **argv) {
  char *path = NULL;
  if (argc == 2) {
    path = argv[1];
  }
  std::vector<void (*)(std::vector<int> &)> funcs = {stoodqsort, myqsort, heapsort, mergesort, nlognsort};
  std::vector<std::string> func_names = {"Std qsort", "My qsort", "Heap sort", "Merge sort", "Worst case nlogn"};
  //names of sorting functions
  std::vector<std::vector<double>> times;
  //array of sort times for each compared function
  for (auto &func : funcs) {
    times.push_back(testme(func));
  }
  Sort_helper::output_sorts(path, func_names, times);
  return 0;
}
